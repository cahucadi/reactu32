import React from 'react'
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import Sidebar from '../../components/Sidebar';
import Header from '../../components/Header';

const Dashboard = () => {
  return (
        <div className='wrapper'>
            
            <Navbar></Navbar>

            <Sidebar></Sidebar>
 
            
            <div className='content-wrapper'>
              
            <Header
              title={"Dashboard"}
              module={""}
            ></Header>

            </div>

            
            <Footer></Footer>
        </div>
  )
}

export default Dashboard;