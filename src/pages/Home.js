import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../utils/APIInvoke';

const Home = () => {

    const [posts, setPosts] = useState(
        []
    );
    
    const loadPosts = async () => {
        const response = await APIInvoke.invokeGET(`/post/list`);
        setPosts(response);
    };
    
    useEffect(() => {
        loadPosts();
    }, []);


    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to={"#"}>Navbar</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to={"#"}>Home <span className="sr-only">(current)</span></Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={"/login"}>Login </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={"/register"}>Register</Link>
                        </li>
                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>

            <br/>
            <div className='container'>

                {
                    posts.map(
                        item =>
                        <div className="card" style={{width: '90%'}}>
                            <div className="card-body">
                                <h5 className="card-title"> { item.title }</h5>
                                <p className="card-text">
                                    { item.content }
                                </p>
                                <Link to={"#"} className="card-link">View</Link>
                            </div>
                        </div>
                    )
                }

            </div>


        </div>
    );
}

export default Home;